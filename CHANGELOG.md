## [1.5.3](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.5.2...v1.5.3) (2021-03-22)


### Bug Fixes

* Update mariadb yum config ([bc5fdb3](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/bc5fdb3))

## [1.5.2](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.5.1...v1.5.2) (2021-03-01)


### Bug Fixes

* Add graylog v4.0 repo ([a80cbe4](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/a80cbe4))
* Add mariadb apt repo ([cdd0f08](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/cdd0f08))
* Add mariadb yum repo ([4eb50df](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/4eb50df))
* Add mongodb apt repo ([41ddfb2](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/41ddfb2))
* Add mongodb yum repo ([01a17d7](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/01a17d7))

## [1.5.1](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.5.0...v1.5.1) (2021-02-18)


### Bug Fixes

* Add kubernetes apt repo ([bdc7234](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/bdc7234))
* Add kubernetes yum repo ([3838b00](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/3838b00))

# [1.5.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.4.3...v1.5.0) (2020-12-01)


### Features

* Update that adds Debian 10 repo ([d52a45a](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/d52a45a))

## [1.4.3](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.4.2...v1.4.3) (2020-10-16)


### Bug Fixes

* Add initial freebsd-pkgs config ([c9da210](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/c9da210))

## [1.4.2](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.4.1...v1.4.2) (2020-10-02)


### Bug Fixes

* Change debian image provider ([8605381](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/8605381))
* Update debian image config ([37fa02c](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/37fa02c))
* Update fedora image config ([7da0d49](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/7da0d49))
* Update ubuntu image config ([46a3a5f](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/46a3a5f))

## [1.4.1](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.4.0...v1.4.1) (2020-06-17)


### Bug Fixes

* Correct graylog repo config error ([97a7024](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/97a7024))

# [1.4.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.3.0...v1.4.0) (2020-05-20)


### Features

* Add graylog repo config ([1f5167e](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/1f5167e))

# [1.3.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.2.0...v1.3.0) (2020-05-19)


### Features

* Add grafana repo config ([70f41eb](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/70f41eb))

# [1.2.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.1.5...v1.2.0) (2020-05-18)


### Features

* Create config for gitlab docker images ([d2d74d5](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/d2d74d5))

## [1.1.5](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.1.4...v1.1.5) (2020-03-31)


### Bug Fixes

* Correct Centos 8 image line ([07a1931](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/07a1931))

## [1.1.4](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.1.3...v1.1.4) (2020-03-30)


### Bug Fixes

* Update pypi repo provider.conf ([45a6024](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/45a6024))

## [1.1.3](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.1.2...v1.1.3) (2020-03-06)


### Bug Fixes

* Correct error in centos image config ([fabb6d0](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/fabb6d0))
* Update centos image config ([721491d](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/721491d))
* Update ubuntu images config ([f572ea7](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/f572ea7))

## [1.1.2](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.1.1...v1.1.2) (2020-03-06)


### Bug Fixes

* Update zabbix config ([90d2cad](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/90d2cad))

## [1.1.1](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.1.0...v1.1.1) (2020-01-08)


### Bug Fixes

* update pypi repo provider ([17bb2ab](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/17bb2ab))

# [1.1.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/compare/v1.0.0...v1.1.0) (2020-01-07)


### Features

* add adamtornhill/code-maat github repo ([64decb0](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/64decb0))

# 1.0.0 (2019-11-05)


### Bug Fixes

* Add rabbitmq-server repo ([22ef6cb](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/22ef6cb))
* Added .mdlrc with rule exceptions ([77c5658](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/77c5658))
* Corrected markdownlint error ([48ced6e](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/48ced6e))


### Features

* Bootstrap CI ([2233b54](https://gitlab.com/dreamer-labs/bashellite/bashellite-configs/commit/2233b54))
